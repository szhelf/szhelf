def bin2dec(binary, LEN):
    dec = 0
    for i in range(LEN):
        dec = dec * 2 + binary[i]
    return dec


# last part of day 1
def Day1(inputfile):
    print('Day 1')
    with open(inputfile, 'r') as f:
        lines = f.readlines()
        f.close()

    firstline = 2  # number of lines to skip
    exline = ""
    exexline = ""
    sum = 0
    exsum = 0
    increased = 0
    for line in lines:
        if (firstline == 0):
            sum = int(line) + int(exline) + int(exexline)
            if (sum > exsum) and (exsum > 0):
                increased = increased + 1
            print(f'sums: {sum} {exsum} {increased}')
        else:
            print(f'firstline: {line}')
            firstline = firstline - 1
        exexline = exline
        exline = line
        exsum = sum

    print(f'Increased: {increased}')


# last part of day 2
def Day2(inputfile):
    print('Day 2')
    forward = 0
    updown = 0
    aim = 0

    with open(inputfile, 'r') as f:
        lines = f.readlines()
        f.close()

    for line in lines:
        words = line.split()
        num = int(words[1])
        if (words[0] == 'forward'):
            forward = forward + num
            updown = updown + (num * aim)
        if (words[0] == 'up'):
            # updown = updown - num
            aim = aim - num
        if (words[0] == 'down'):
            # updown = updown + num
            aim = aim + num

    print(f'Result: {forward * updown} forward: {forward} updown: {updown}')


# day 3 part 1
def Day3(inputfile):
    print('Day 3')
    LINELEN = 12
    gammarate = 0
    epsilonrate = 0
    zeros = [0] * LINELEN
    ones = [0] * LINELEN
    common = [0] * LINELEN
    leastcommon = [0] * LINELEN

    with open(inputfile, 'r') as f:
        lines = f.readlines()
        f.close()

    for line in lines:
        for i in range(LINELEN):
            c = line[i]
            if (c == '0'):
                zeros[i] = zeros[i] + 1
            if (c == '1'):
                ones[i] = ones[i] + 1

    for i in range(LINELEN):
        if (ones[i] > zeros[i]):
            common[i] = 1
        else:
            leastcommon[i] = 1

    print(f'Common: {common} Least common: {leastcommon}')

    gammarate = bin2dec(common, LINELEN)
    epsilonrate = bin2dec(leastcommon, LINELEN)

    print(f'Gamma rate: {gammarate} Epsilon rate: {epsilonrate} Result: {gammarate * epsilonrate}')


if __name__ == '__main__':
    # Day1('input.txt')
    # Day2('input2.txt')
    Day3('input3.txt')
